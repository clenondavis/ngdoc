/**
 * @ngdoc object
 * @name  app.dashboard
 * @description
 *
 * This is the main module for the spa.
 */
(function(){
	'use strict';

	angular
		.module('app', []);
})();