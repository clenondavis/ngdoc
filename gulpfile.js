var gulp  = require('gulp');
var gulpDocs = require('gulp-ngdocs');
var watch = require('gulp-watch');

gulp.task('ngdocs', [], function() {
   
    var options = {    	
    	html5Mode: false,
    	startPage: '/api/module.app',
    	title: "CRMX2.0",
    };
    return gulp
    	.src('app/*.js')
    	.pipe(gulpDocs.process(options))
    	.pipe(gulp.dest('./docs'))
});

gulp.task('watch', function() {
    gulp.watch('app/*.js', ['ngdocs']);
});

gulp.task('default', ['ngdocs']);